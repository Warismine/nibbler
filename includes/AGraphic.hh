#ifndef AGRAPHIC_HH_
# define AGRAPHIC_HH_

# include <iostream>
# include "Map.hh"
# include "DLLoader.hh"

class AGraphic {
private:
  AGraphic	&operator=(const AGraphic &);
  AGraphic(const AGraphic &);
protected:
  const Map	*_map;
  kevent	_current_event;
  size_t	_score;

public:
  AGraphic(const Map &map, kevent event) : _map(&map), _current_event(event),
					   _score(0) {};
  virtual		~AGraphic() {};
  virtual void		Move(const t_pos &current) = 0;
  virtual kevent	getKeyEvent() = 0;
  virtual void		quit() = 0;
  virtual void		playOpening() = 0;
  virtual void		setScore(size_t score) {
    _score = score;
  };
};

#endif
