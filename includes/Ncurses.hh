
#ifndef NCURSES_HPP_
# define NCURSES_HPP_

# include <iostream>
# include <ncurses.h>
# include <pthread.h>
# include "../includes/AGraphic.hh"

class	Ncurses : public AGraphic
{
public:
  /* Constructors &&  Destructors */

  Ncurses(const Map &, kevent);
  ~Ncurses();

  /* Getters */
  virtual kevent	getKeyEvent();
  int			getGet() const;
  const t_pos		&getPos() const;

  /* Member functions */
  virtual void		playOpening() {}
  virtual void		quit();
  virtual void		Move(const t_pos &);
  Ncurses		&operator=(const Ncurses &);
  void			dispMap() const;
  void			dispCase(int, int, const Block &) const;
  void			dispScore() const;
  bool			checkEvent(const kevent &, const kevent &) const;

private:
  size_t		_get;
  t_pos			_pos;
  typedef struct	s_color
  {
    short		pair;
    short		color;
  }			t_color;
};

#endif /* !NCURSES_HPP_ */
