#ifndef FRAME_H
#define FRAME_H

# define MIN(X, Y)	(X > Y ? Y : X)
# define MAX(X, Y)	(X < Y ? Y : X)

class Frame
{
public:
  Frame(const std::string &path) {
    sf::VideoMode	vid = sf::VideoMode::getDesktopMode();
    float		scale;

    _tex.loadFromFile(path);
    scale = static_cast<float>(vid.width) / _tex.getSize().x;
    scale = MIN(scale, static_cast<float>(vid.height) / _tex.getSize().y);
    _tex.setSmooth(true);
    _sp.setTexture(_tex);
    _sp.setScale(scale, scale);
  }

  const sf::Sprite &getSprite() {return _sp;}

private:
  sf::Texture	_tex;
  sf::Sprite	_sp;
};

#endif /* FRAME_H */
