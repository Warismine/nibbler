#ifndef LIBOGL_HH_
# define LIBOGL_HH_

# include "AGraphic.hh"
# include "sdlglutils.h"

# define MAX(X, Y)	(X > Y ? X : Y)
# define GL_IMG(X)	"./res/Opengl/" X


class LibOGL : public AGraphic
{
public:
  LibOGL(const Map &map, kevent event);
  virtual		~LibOGL();
  virtual void		Move(const t_pos &current);
  virtual kevent	getKeyEvent();
  virtual void		quit();
  virtual void		playOpening() {}

private:
  void			affScore() const;
  void			printCube(int x, int y) const;
  void			printSphere(int x, int y, float r) const;
  void			changeSnakeColor(content part,
					 unsigned char num) const;
  void			TextureMode(bool flag) const;

  GLuint		_ground;
  GLuint		_nazi;
  GLuint		_illuminati;
  GLuint		_aigle;
  GLuint		_croixDeFer;
  GLuint		_snake;
};

#endif /* LIBOGL_HH_ */
