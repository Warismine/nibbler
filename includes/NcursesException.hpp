#ifndef NCURSESEXCEPTION_HPP_
# define NCURSESEXCEPTION_HPP_

#include <iostream>

class NcursesException : public std::exception {
public:
  NcursesException(const std::string &) throw () {};
  ~NcursesException() throw () {};
  virtual const char	*what() const throw () {
    return ("");
  };

  class InitScrFailed
  {
  public:
    InitScrFailed() throw () {};
    virtual const char	*what() const throw () {
      return ("An errror occured: Initscr Failed.\n");
    };
  };

  class KeypadFailed
  {
  public:
    KeypadFailed() throw () {};
    virtual const char	*what() const throw () {
      return ("An errror occured: KeypadFailed.\n");
    };
  };

  class StartColorFailed
  {
  public:
    StartColorFailed() throw () {};
    virtual const char	*what() const throw () {
      return ("An errror occured: StartColorFailed.\n");
    };
  };

  class RawFailed
  {
  public:
    RawFailed() throw () {};
    virtual const char	*what() const throw () {
      return ("An errror occured: Raw Failed./n");
    };
  };

  class EndWinFailed
  {
  public:
    EndWinFailed() throw () {};
    virtual const char	*what() const throw () {
      return ("An errror occured: Raw Failed./n");
    };
  };

private:
  const std::string	msg;
};

#endif /* !NCURSESEXCEPTION_HPP_ */
