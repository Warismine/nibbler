#ifndef SOCKET_HH_
# define SOCKET_HH_

# include <iostream>
# include <sys/types.h>
# include <sys/socket.h>
# include <netdb.h>
# include <netinet/in.h>
# include <arpa/inet.h>
# include <unistd.h>
# include <list>
# include "SocketException.hpp"

# define READ 1
# define WRITE 2

class Socket {
public:
  Socket(const std::string &port, const std::string &ip = "");
  Socket() {};
  Socket(const Socket &);
  Socket	&operator=(const Socket &);
  ~Socket() {};

  int			create_socket();

  void			set_socket_domain(int domain);
  void			set_socket_type(int type);
  void			set_socket_protocole(const char *protocole);

  int			get_socket_domain() const;
  int			get_socket_type() const;
  char			*get_socket_protocole() const;

  int			bind_socket();
  int			connect_socket();

  void			set_sin_family(short sin_family);
  void			set_string_sin_port(const char *sin_port);
  void			set_int_sin_port(int sin_port);
  void			set_string_sin_addr(const char *sin_addr);
  void			set_sin_addr(in_addr_t sin_addr);

  short			get_sin_family() const;
  unsigned short	get_sin_port() const;
  in_addr_t		get_sin_addr() const;

  int			accept_socket(struct sockaddr_in *s_client);
  int			listen_socket(int client_nb);

  int			close_socket();
  void			retrieve_data(int fd, size_t size, void *to, int flag) const;
  void			send_data(int fd, size_t size, void *to, int flag) const;
  int			get_socket() const;

private:
  int			_socket_fd;
  int			_type;
  int			_domain;
  struct protoent	*_ep;
  struct sockaddr_in	_s_in;
  bool			_socket_connected;
  std::list<int>	_clients_fd;
};

#endif /* !SOCKET_HH_ */
