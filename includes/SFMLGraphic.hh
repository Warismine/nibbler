#ifndef SFMLGRAPHIC_HH_
# define SFMLGRAPHIC_HH_

# include <list>
# include <map>
# include "SFML/Graphics.hpp"
# include "SFML/Audio.hpp"
# include "AGraphic.hh"
# include "Frame.hpp"
# include "CenterSprite.hpp"

# define TOPBAR		40
# define CASEW(X, Y)	(X * 80 * Y)
# define CASEH(X, Y)	(X * 80 * Y + TOPBAR)
# define SFML_IMG(X)	"./res/SFML/imgs/" X
# define SFML_FONT(X)	"./res/SFML/fonts/" X
# define SFML_MOV(X)	"./res/SFML/movie/" X
# define SFML_SND(X)	"./res/SFML/audio/" X

# define ABSSUB(X, Y)	(MAX(X, Y) - MIN(X, Y))

# define ISKEY(E)	(E.type == sf::Event::KeyPressed)
# define ISJOY(E)	(E.type == sf::Event::JoystickButtonPressed)

# define PASSKEY(E)	(ISKEY(E) && E.key.code == sf::Keyboard::Space)
# define PASSJOY(E)	(ISJOY(E) && E.joystickButton.button == 2)
# define PASS(E)	(PASSKEY(E) || PASSJOY(E))

# define QUITKEY(E)	(ISKEY(E) && E.key.code == sf::Keyboard::Q)
# define QUITJOY(E)	(ISJOY(E) && E.joystickButton.button == 1)
# define QUIT(E)	(QUITKEY(E) || QUITJOY(E))

class SFMLGraphic : public AGraphic {

public:
  SFMLGraphic(const Map &map, kevent event);
  virtual		~SFMLGraphic();
  virtual void		Move(const t_pos &current);
  virtual kevent	getKeyEvent();
  virtual void		quit();
  virtual void		playOpening();

private:
  std::map<kevent, float>		_angle;
  std::map<content, CenterSprite*>	_sprites;
  std::vector<sf::Color>		_color;
  sf::RenderWindow			_window;
  CenterSprite				_bg;
  float					_scale;
  sf::Font				_font;
  sf::SoundBuffer			_buffer;
  sf::Sound				_music;
  std::list<Frame*>			_frames;
  bool					_stop;

  void		print_bg();
  void		drawAtCase(int x, int y, const Block &block);
  void		affScore();
  float		getPrevDirection(int x, int y, kevent dir);
  void		readFrames();
  void		printLoading();
};

#endif
