#ifndef MAP_HH_
# define MAP_HH_

typedef enum	__attribute__((__packed__)) e_content {
  EMPTY = 0,
  SNAKE_HEAD,
  SNAKE_BODY,
  SNAKE_TAIL,
  FOOD,
  XFOOD,
  WALL,
  KIHOHA,
  STRAIGHT
}		content;

typedef enum	__attribute__((__packed__)) e_kevent
  {
    NONE = 0,
    LEFT,
    RIGHT,
    UP,
    DOWN,
    SWITCH1,
    SWITCH2,
    SWITCH3,
    QUIT
  }		kevent;

typedef struct  s_size
{
  int		w;
  int		h;
}		t_size;

typedef struct	__attribute__((__packed__)) s_pos
{
  int		x;
  int		y;
}		t_pos;

typedef struct	__attribute__((__packed__)) s_way
{
  unsigned char	player;
  char		num;
  content	part;
}		t_way;

typedef struct	__attribute__((__packed__)) s_block
{
  t_way		body;
  kevent	direction;
}		Block;

typedef struct	__attribute__((__packed__)) s_map {
  t_size	size;
  Block		field[60][60];
}		Map;

#endif
