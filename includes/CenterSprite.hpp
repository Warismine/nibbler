#ifndef CENTERSPRITE_HPP_
# define CENTERSPRITE_HPP_

# include "SFML/Graphics.hpp"
# include <string>
# include <iostream>

class CenterSprite : public sf::Sprite {
public:
  CenterSprite(const std::string &path) :
    Sprite()
  {
    _img.loadFromFile(path);
    _img.setSmooth(true);
    this->setTexture(_img);
    this->setOrigin(getW()/2, getH()/2);
  }

  virtual ~CenterSprite() {}

  void setPosition(float x, float y) {
    sf::Sprite::setPosition(x + getW() / 2,
			    y + getH() / 2);
  }

  int getW() {
    return _img.getSize().x * this->getScale().x;
  }

  int getH() {
    return _img.getSize().y * this->getScale().y;
  }

  int getX() {
        return getPosition().x - getW() / 2;
  }

  int getY() {
    return getPosition().y - getH() / 2;
  }
private:
  sf::Texture _img;
};

#endif
