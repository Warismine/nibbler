#ifndef DLLOADER_EXCEPTION_HPP_
# define DLLOADER_EXCEPTION_HPP_

#include <iostream>

class DLLoaderException : public std::exception {
public:
  DLLoaderException(const std::string &) throw () {};
  ~DLLoaderException() throw() {};
  virtual const char	*what() const throw () {
    return ("");
  };

  class DLOpenFailed
  {
  public:
    DLOpenFailed() throw () {};
    virtual const char	*what() const throw () {
      return ("An error occured: Dlopen Failed.\n");
    };
  };

  class DLCloseFailed
  {
  public:
    DLCloseFailed() throw () {};
    virtual const char	*what() const throw () {
      return ("An error occured: Dlclose Failed.\n");
    };
  };

  class DLSymFailed
  {
  public:
    DLSymFailed() throw () {};
    virtual const char	*what() const throw () {
      return ("An error occured: Dlsym Failed.\n");
    };
  };
};

# endif
