#ifndef GAME_HH_
# define GAME_HH_

# include <iostream>
# include <pthread.h>
# include "AGraphic.hh"
# include "Socket.hh"
# include "NcursesException.hpp"
typedef AGraphic *(*graphic)(const Map &, kevent);

typedef struct	s_snake_player SnakePlayer;

typedef	struct		t_data
{
  Map			map;
  bool			is_dead;
  bool			end_of_game;
}			Data;

typedef struct		s_player {
  size_t		speed;
  size_t		score;
  size_t		player_nb;
  t_pos			current;
  kevent		event;
  SnakePlayer		*snake_player;
  struct sockaddr_in	s_in;
  bool			is_alive;
  int		        fd;
  Data			data;
}			Player;

class Game {
public:
  Game(const t_size &size, const std::string &lib);
  Game(const std::string &lib, Socket *sock);
  Game(const t_size &size, const std::string &lib, Socket *sock, int player_nb);
  ~Game();
  void		setLib(const AGraphic &);
  void		switchLib(int, kevent);
  static void	*Move(void *);
  static void	*launchBall(void *);
  bool		launchBallToStorm();
  bool		isCorrectWay(kevent event, Player *player);
  void		initMap();
  void		initSnake(Player &player, int num);
  void	        snakeOne(Player &player);
  void		snakeTwo(Player &player);
  void	        snakeThree(Player &player);
  void	        snakeFour(Player &player);
  bool		isAGoodMove(kevent old, kevent new_e) const;
  bool		isOnBorder(int, int) const;
  void		quitGame(Player *player);
  bool		isChangingLib(kevent) const;
  int		toInt(kevent) const;
  bool		canBeHere(content) const;
  void		updatePosition(bool, kevent, Player *player);
  bool		onFood(const Player *) const;
  bool		isTheSnakeHead(int, int, const Player&) const;
  bool		getPart(t_pos &pos, int n, const Player *player);
  void		printMap() const;
  void		changeSnakeSpeed(Player *player) const;
  size_t	getScoreForPlayer(Player *player) const;
  void		setScoreWithFood(content, Player *player);
  static void	*putFoodOnMap(void *);
  static void	*refreshTime(void *);
  static void	*putXtraFoodOnMap(void *);
  void		increaseSnakeSize(kevent direction, Player *player,
				  content food, t_pos &pos);
  AGraphic	*getGraph() const;
  Map		*getMap();
  void		initPlayer(int num);
  void		retrieveData(int fd = 0, int flag = 0, int num = 0);
  void		sendData(int fd, int num = 0);
  kevent	getEvent(SnakePlayer *);
  void		clean_game();
  static void	horizontalLaunch(Map *map, kevent event, int dir, const t_pos &pos);
  static void   verticalLaunch(Map *map, kevent event, int dir, const t_pos &pos);
  bool		isOnline() const;

private:
  Map		_map;
  Socket	*_sock;
  AGraphic	*_graph;
  Player	_players[4];
};

typedef struct	s_snake_player {
  Game		*game;
  Player	*player;
}		SnakePlayer;

#endif
