#ifndef SOCKETEXCEPTION_HPP_
# define SOCKETEXCEPTION_HPP_

#include <iostream>

class SocketException : public std::exception {
public:
  SocketException(const std::string &) throw () {};
  ~SocketException() throw () {};
  virtual const char	*what() const throw () {
    return ("");
  };

  class SocketFailed
  {
  public:
    SocketFailed() throw () {};
    virtual const char	*what() const throw () {
      return ("An error occured: Socket Failed.\n");
    };
  };

  class SocketClose
  {
  public:
    SocketClose() throw () {};
    virtual const char	*what() const throw () {
      return ("An error occured: Closing socket Failed.\n");
    };
  };

  class GetprotobynameFailed
  {
  public:
    GetprotobynameFailed() throw () {};
    virtual const char	*what() const throw () {
      return ("An error occured: Getprotobyname Failed./n");
    };
  };

  class BindFailed
  {
  public:
    BindFailed() throw () {};
    virtual const char	*what() const throw () {
      return ("An error occured: Bind Failed");
    };
  };

  class ConnectFailed
  {
  public:
    ConnectFailed() throw () {};
    virtual const char	*what() const throw () {
      return ("An error occured: Connect Failed");
    };
  };

  class RecvFailed
  {
  public:
    RecvFailed() throw () {};
    virtual const char	*what() const throw () {
      return ("An error occured: Recv Failed");
    };
  };

  class SendFailed
  {
  public:
    SendFailed() throw () {};
    virtual const char	*what() const throw () {
      return ("An error occured: Send Failed");
    };
  };

  class ListenFailed
  {
  public:
    ListenFailed() throw () {};
    virtual const char	*what() const throw () {
      return ("An error occured: Listen Failed");
    };
  };

  class AcceptFailed
  {
  public:
    AcceptFailed() throw () {};
    virtual const char	*what() const throw () {
      return ("An error occured: Accept Failed");
    };
  };

private:
  const std::string	msg;
};
#endif /* !SOCKETEXCEPTION_HPP_ */
