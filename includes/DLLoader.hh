#ifndef DLLOADER_HPP_
# define DLLOADER_HPP_

# include <map>
# include <iostream>
# include <dlfcn.h>
# include "AGraphic.hh"
# include "DLLoaderException.hpp"

template <typename X>
class DLLoader {
public:
  static DLLoader   &Instance();
  void		initWithLib(const std::string &lib);
  void		closeLib();
  void		changeLib(const std::string &);
  void		*getFunctionFromLib(const std::string &) const;
private:
  DLLoader() {};
  ~DLLoader() {};
  DLLoader(const DLLoader &);
  DLLoader	&operator=(const DLLoader &);
  void		*_currentHandler;
  std::map<std::string, void *>	handler;
};

#endif
