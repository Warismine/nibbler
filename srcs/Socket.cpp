#include <sstream>
#include <algorithm>
#include "Socket.hh"

Socket::Socket(const Socket &sock) {
  if (this == &sock)
    return ;
  _type = sock._type;
  _domain = sock._domain;
  _ep = new struct protoent;
  std::copy(sock._ep, sock._ep + sizeof(struct protoent), _ep);
  _s_in = sock._s_in;
  _socket_connected = false;
  _socket_fd = 0;
  _clients_fd.clear();
  return ;
}

Socket	&Socket::operator=(const Socket &sock) {
  if (this == &sock)
    return (*this);
  _type = sock._type;
  _domain = sock._domain;
  _ep = new struct protoent;
  std::copy(sock._ep, sock._ep + sizeof(struct protoent), _ep);
  _s_in = sock._s_in;
  _socket_connected = false;
  _socket_fd = 0;
  _clients_fd.clear();
  return (*this);
}

Socket::Socket(const std::string &port, const std::string &ip) {
  _socket_connected = false;
  set_socket_domain(AF_INET);
  set_socket_type(SOCK_STREAM);
  set_socket_protocole("TCP");
  create_socket();
  set_sin_family(AF_INET);
  if (ip.empty())
    set_sin_addr(INADDR_ANY);
  else
    set_string_sin_addr(ip.c_str());
  set_string_sin_port(port.c_str());
}

int		Socket::create_socket()
{
  if (_ep == NULL)
    throw (SocketException::SocketFailed());
  _socket_fd = socket(_domain, _type, _ep->p_proto);
  _socket_connected = true;
  if (_socket_fd == -1)
    throw (SocketException::SocketFailed());
  return (_socket_fd);
}

void		Socket::set_socket_domain(int domain)
{
  _domain = domain;
}

void		Socket::set_socket_type(int type)
{
  _type = type;
}

void		Socket::set_socket_protocole(const char *protocole)
{
  if ((_ep = getprotobyname(protocole)) == NULL)
    throw (SocketException::GetprotobynameFailed());
}

int		Socket::close_socket()
{
  if (_socket_connected)
    return (0);
  if (close(_socket_fd) == -1)
    throw (SocketException::SocketClose());
  _socket_connected = false;
  return (0);
}

int		Socket::get_socket_domain() const
{
  return (_domain);
}

int		Socket::get_socket_type() const
{
  return (_type);
}

char		*Socket::get_socket_protocole() const
{
  return (_ep->p_name);
}

void		Socket::set_sin_family(short sin_family)
{
  _s_in.sin_family = sin_family;
}

void		Socket::set_string_sin_port(const char *sin_port)
{
  int		num;
  std::istringstream is;

  is.str(sin_port);
  is >> num;
  _s_in.sin_port = htons(num);
}

void		Socket::set_int_sin_port(int sin_port)
{
  _s_in.sin_port = htons(sin_port);
}

void		Socket::set_string_sin_addr(const char *sin_addr)
{
  _s_in.sin_addr.s_addr = inet_addr(sin_addr);
}

void		Socket::set_sin_addr(in_addr_t sin_addr)
{
  _s_in.sin_addr.s_addr = sin_addr;
}

int		Socket::bind_socket()
{
  if ((bind(_socket_fd, reinterpret_cast<const struct sockaddr *>(&_s_in),
	       sizeof(_s_in))) == -1)
    {
      close_socket();
      _socket_connected = false;
      throw (SocketException::BindFailed());
    }
  return (0);
}

int		Socket::connect_socket()
{
  if (_socket_connected == true)
    close_socket();
  _socket_connected = true;
  if ((connect(_socket_fd, reinterpret_cast<const struct sockaddr *>(&_s_in),
	       sizeof(_s_in))) == -1)
    {
      close_socket();
      _socket_connected = false;
      throw (SocketException::ConnectFailed());
      return (-1);
    }
  return (0);
}

short		Socket::get_sin_family() const
{
  return (_s_in.sin_family);
}

unsigned short	Socket::get_sin_port() const
{
  return (_s_in.sin_port);
}

in_addr_t	Socket::get_sin_addr() const
{
  return (_s_in.sin_addr.s_addr);
}

void		Socket::retrieve_data(int fd, size_t size, void *to, int flag) const
{
  char		buff[size];
  char		*copy;
  size_t	len;
  size_t	ret;

  len = 0;
  ret = 0;
  std::fill(buff, buff + size, 0);
  while (len < size)
    {
      ret = recv(fd, &buff[len], size, flag);
      if (ret == static_cast<size_t>(-1) ||
	  ret == static_cast<size_t>(0))
	throw (SocketException::RecvFailed());
      len += ret;
    }
  if (len > size)
    return ;
  copy = static_cast<char *>(to);
  std::copy(buff, buff + size, copy);
}

void		Socket::send_data(int fd, size_t size, void *to, int flag) const {
  if (send(fd, to, size, flag) < 0)
    throw (SocketException::SendFailed());
}

int		Socket::listen_socket(int client_nb)
{
  if ((listen(_socket_fd, client_nb)) == -1)
    {
      close_socket();
      throw (SocketException::ListenFailed());
    }
  return (0);
}

int		Socket::accept_socket(struct sockaddr_in *s_client)
{
  int		client_fd;
  socklen_t	s_len_client;

  s_len_client = sizeof(*s_client);
  client_fd = accept(_socket_fd, reinterpret_cast<struct sockaddr *>(s_client),
		     &s_len_client);
  if (client_fd >= 0)
    _clients_fd.push_back(client_fd);
  else
    throw (SocketException::AcceptFailed());
  return (client_fd);
}

int	       	Socket::get_socket() const {
  return (_socket_fd);
}
