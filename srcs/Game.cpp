#include <cstdlib>
#include "Game.hh"

Game::Game(const std::string &lib, Socket *sock) {
  kevent		tmp;
  graphic		ptr;
  pthread_t		time;
  DLLoader<AGraphic *>	*dl = &(DLLoader<AGraphic *>::Instance());

  _sock = sock;
  _sock->connect_socket();
  try { retrieveData(); }
  catch(SocketException::RecvFailed &e) {
    std::cerr << e.what() << std::endl;
    return ;
  }
  dl->initWithLib(lib);
  ptr = reinterpret_cast<graphic>(dl->getFunctionFromLib("getInstance"));
  _map = _players[0].data.map;
  _graph = ptr(_map, _players[0].event);
  _graph->Move(_players[0].current);
  try { retrieveData(); }
  catch(SocketException::RecvFailed &e) {
    std::cerr << e.what() << std::endl;
    clean_game();
    return ;
  }
  while (!_players[0].data.end_of_game)
    {
      try { retrieveData(); }
      catch(SocketException::RecvFailed &e) {
	std::cerr << e.what() << std::endl;
	clean_game();
	return ;
      }
      _map = _players[0].data.map;
      pthread_create(&time, NULL, &Game::refreshTime, &_players[0]);
      _graph->Move(_players[0].current);
      pthread_join(time, NULL);
      tmp = _graph->getKeyEvent();
      if (isChangingLib(tmp))
        switchLib(toInt(tmp) % SWITCH1, _players[0].event);
      else
	_players[0].event = tmp;
      _graph->setScore(getScoreForPlayer(&_players[0]));
      try { sendData(_sock->get_socket()); }
      catch(SocketException::SendFailed &e) {
	std::cerr << e.what() << std::endl;
	clean_game();
	return ;
      }
    }
  clean_game();
}

Game::Game(const t_size &size, const std::string &lib, Socket *sock, int player_nb) {
  graphic		ptr;
  pthread_t		thread[4];
  DLLoader<AGraphic *>	*dl = &(DLLoader<AGraphic *>::Instance());

  _sock = sock;
  _sock->bind_socket();
  _sock->listen_socket(player_nb - 1);
  for (int i = 0; i < 4; ++i) {
    _players[i].data.is_dead = true;
    _players[i].fd = 0;
  }
  initPlayer(0);
  dl->initWithLib(lib);
  _map.size = size;
  ptr = reinterpret_cast<graphic>(dl->getFunctionFromLib("getInstance"));
  initMap();
  initSnake(_players[0], 0);
  _players[0].fd = 0;
  _graph = ptr(_map, _players[0].event);
  _map.field[_map.size.h / 2][_map.size.w / 2].body.part = XFOOD;
  for (int i = 1; i < player_nb; ++i) {
    _players[i].fd = _sock->accept_socket(&_players[i].s_in);
    initPlayer(i);
    initSnake(_players[i], i);
    _players[i].data.map = _map;
    try { sendData(_players[i].fd, i); }
    catch(SocketException::SendFailed &e) {
      std::cerr << e.what() << std::endl;
      return ;
    }
    _graph->Move(_players[i].current);
  }
  sleep(1);
  Game::putFoodOnMap(&_map);
  Game::putFoodOnMap(&_map);
  for (int i = 0; i < player_nb; ++i) {
    if (i != 0)
      {
	try { sendData(_players[i].fd, i); }
	catch(SocketException::SendFailed &e) {
	  std::cerr << e.what() << std::endl;
	  return ;
	}
      }
    if (pthread_create(&thread[i], NULL, &Game::Move, _players[i].snake_player) != 0)
      {
	std::cerr << "Couldn't be able to create thread" << std::endl;
	return ;
      }
  }
  pthread_join(thread[0], NULL);
  for (int i = 1; i < player_nb; ++i) {
    if (_players[i].fd != 0)
      close(_players[i].fd);
    _players[i].fd = 0;
  }
}

Game::Game(const t_size &size, const std::string &lib)
{
  graphic		ptr;
  pthread_t		thread;
  DLLoader<AGraphic *>	*dl = &(DLLoader<AGraphic *>::Instance());

  for (int i = 0; i < 4; ++i) {
    _players[i].data.is_dead = true;
  }
  _sock = NULL;
  initPlayer(0);
  try { dl->initWithLib(lib); }
  catch (DLLoaderException::DLOpenFailed &e) {
    std::cerr << e.what() << std::endl;
    return ;
  }
  _map.size = size;
  try {
    ptr = reinterpret_cast<graphic>(dl->getFunctionFromLib("getInstance")); }
  catch (DLLoaderException::DLCloseFailed &e) {
    std::cerr << e.what() << std::endl;
    return ;
  }
  catch (DLLoaderException::DLSymFailed &e) {
    std::cerr << e.what() << std::endl;
    return ;
  }
  initMap();
  initSnake(_players[0], 0);
  _graph = ptr(_map, _players[0].event);
  _graph->playOpening();
  Game::putFoodOnMap(&_map);
  Game::putFoodOnMap(&_map);
  if (pthread_create(&thread, NULL, &Game::Move, _players[0].snake_player) != 0)
    {
      std::cerr << "Couldn't be able to create thread" << std::endl;
      return ;
    }
  pthread_join(thread, NULL);
}

void	Game::retrieveData(int fd, int flag, int num) {
  if (fd == 0)
    _sock->retrieve_data(_sock->get_socket(), sizeof(Player), &(_players[num]), flag);
  else
    _sock->retrieve_data(fd, sizeof(Player), &(_players[num]), flag);
}

void	Game::sendData(int fd, int num) {
  _sock->send_data(fd, sizeof(Player),  &(_players[num]), 0);
}

void	Game::printMap() const {
  for (int y = 0; y < _map.size.h; ++y)
    {
      for (int x = 0; x < _map.size.w; ++x)
	std::cout << _map.field[y][x].body.part;
      std::cout << std::endl;
    }
  std::cout << std::endl;
}

void		Game::clean_game() {
  DLLoader<AGraphic *>	*dl = &(DLLoader<AGraphic *>::Instance());

  try {
    if (_sock)
      _sock->close_socket();
  }
  catch (SocketException::SocketClose &e) {
    std::cerr << e.what() << std::endl;
    _players[0].data.is_dead = true;
    pthread_exit(NULL);
  }
  _graph->quit();
  delete _graph;
  _graph = NULL;
  try  { dl->closeLib(); }
  catch (DLLoaderException::DLCloseFailed &e) {
    std::cerr << e.what() << std::endl;
    _players[0].data.is_dead = true;
    pthread_exit(NULL);
  }
  return ;
}

void				Game::switchLib(int num, kevent e) {
  AGraphic			*(*ptr)(const Map &, kevent);
  DLLoader<AGraphic *>		*dl = &(DLLoader<AGraphic *>::Instance());
  static const char *const	lib[] = {
    "./lib_nibbler_sfml.so",
    "./lib_nibbler_ncurses.so",
    "./lib_nibbler_opengl.so"
  };

  delete _graph;
  _graph = NULL;
  try {
    dl->closeLib();
    dl->changeLib(lib[num]);
    ptr = reinterpret_cast<graphic>(dl->getFunctionFromLib("getInstance"));
  }
  catch (DLLoaderException::DLOpenFailed &e) {
    std::cerr << e.what() << std::endl;
    _players[0].data.is_dead = true;
    pthread_exit(NULL);
  }
  catch (DLLoaderException::DLCloseFailed &e) {
    std::cerr << e.what() << std::endl;
    _players[0].data.is_dead = true;
    pthread_exit(NULL);
  }
  catch (DLLoaderException::DLSymFailed &e) {
    _players[0].data.is_dead = true;
    std::cerr << e.what() << std::endl;
    pthread_exit(NULL);
  }
  try {
    _graph = ptr(_map, e);
  }
  catch (NcursesException::InitScrFailed &e) {
    _players[0].data.is_dead = true;
    pthread_exit(NULL);
  }
  catch (NcursesException::KeypadFailed &e) {
    _players[0].data.is_dead = true;
    pthread_exit(NULL);
  }
  catch (NcursesException::RawFailed &e) {
    _players[0].data.is_dead = true;
    pthread_exit(NULL);
  }
  catch (NcursesException::StartColorFailed &e) {
    _players[0].data.is_dead = true;
    pthread_exit(NULL);
  }
  _graph->setScore(getScoreForPlayer(&_players[0]));
}

void		*Game::Move(void *arg) {
  kevent	tmp;
  SnakePlayer	*p;

  p = reinterpret_cast<SnakePlayer *>(arg);
  p->player->data.is_dead = false;
  if (p->game->isOnline())
    sleep(3);
  while (1)
    {
      tmp = p->game->getEvent(p);
      if (tmp == QUIT)
	{
	  p->game->quitGame(p->player);
	  return (0);
	}
      if (p->game->isChangingLib(tmp) && p->player->player_nb)
	p->game->switchLib(p->game->toInt(tmp) % SWITCH1, p->player->event);
      else if (p->game->isAGoodMove(p->player->event, tmp))
	p->player->event = tmp;
      if (!p->game->isCorrectWay(p->player->event, p->player))
	{
	  p->game->quitGame(p->player);
	  return (0);
	}
      p->game->updatePosition(p->game->onFood(p->player),
			      p->player->event, p->player);
      p->game->changeSnakeSpeed(p->player);
    }
  return (0);
}

kevent	Game::getEvent(SnakePlayer *p) {
  kevent	current_event;
  kevent	new_event;
  pthread_t	time;

  if (p->player->player_nb > 1)
    {
      p->player->data.map = _map;
      try { sendData(p->player->fd, p->player->player_nb - 1); }
      catch (SocketException::SendFailed &e) {
	std::cerr << e.what() << std::endl;
	p->player->data.is_dead = true;
	_sock->close_socket();
	pthread_exit(NULL);
      }
      current_event = p->player->event;
      try { retrieveData(p->player->fd, 0, p->player->player_nb - 1); }
      catch (SocketException::RecvFailed &e) {
	std::cerr << e.what() << std::endl;
	p->player->data.is_dead = true;
	_sock->close_socket();
        pthread_exit(NULL);
      }
      new_event = p->player->event;
      p->player->event = current_event;
      return (new_event);
    }
  pthread_create(&time, NULL, &Game::refreshTime, p->player);
  p->game->getGraph()->Move(p->player->current);
  pthread_join(time, NULL);
  return (p->game->getGraph()->getKeyEvent());
}

void	*Game::refreshTime(void *arg) {
  Player *p = static_cast<Player *>(arg);

  usleep(p->speed);
  return (0);
}

bool		Game::isCorrectWay(kevent event, Player *player) {
  switch (event) {
  case LEFT:
    if (canBeHere(_map.field[player->current.y][player->current.x - 1].body.part))
      return (!!(player->current.x -= 1));
    return (false);
  case RIGHT:
    if (canBeHere(_map.field[player->current.y][player->current.x + 1].body.part))
      return (!!(player->current.x += 1));
    return (false);
  case UP:
    if (canBeHere(_map.field[player->current.y - 1][player->current.x].body.part))
      return (!!(player->current.y -= 1));
    return (false);
  case DOWN:
    if (canBeHere(_map.field[player->current.y + 1][player->current.x].body.part))
      return (!!(player->current.y += 1));
    return (false);
  default:
    return (false);
  }
  return (false);
}

void		Game::quitGame(Player *player) {
  kevent event;
  bool	end;

  player->data.is_dead = true;
  while (!_players[0].data.end_of_game)
    {
      end = true;
      if (((event = getEvent(player->snake_player)) == QUIT) && (player->player_nb == 1))
	{
	  for (int i = 0; i < 4; ++i) {
	    _players[i].data.end_of_game = true; }
	  clean_game();
	  return ;
	}
      else if (event == QUIT)
	{
	  _players[player->player_nb - 1].data.end_of_game = true;
	  getEvent(player->snake_player);
	  return ;
	}
      for (int i = 0; i < 4; ++i) {
	if (!_players[i].data.is_dead)
	  {
	    end = false;
	    break ;
	  }
      }
      for (int i = 0; i < 4; ++i) {
	_players[i].data.end_of_game = end; }
    }
  getEvent(player->snake_player);
  if (player->player_nb == 1)
    clean_game();
}

bool		Game::isAGoodMove(kevent old, kevent new_e) const {
  if ((old == LEFT) || (old == RIGHT))
    return (!((new_e == LEFT) || (new_e == RIGHT)));
  else if ((old == UP) || (old == DOWN))
    return (!((new_e == UP) || (new_e == DOWN)));
  return (false);
}

bool	Game::canBeHere(content cont) const {
  return (cont == FOOD || cont == EMPTY || cont == XFOOD);
}

bool	Game::isChangingLib(kevent event) const {
  return (((event >= SWITCH1) && (event <= SWITCH3)));
}

int	Game::toInt(kevent event) const {
  return (static_cast<int>(event));
}

bool	Game::onFood(const Player *player) const {
  return (_map.field[player->current.y][player->current.x].body.part == FOOD ||
	  _map.field[player->current.y][player->current.x].body.part == XFOOD);
}

bool	Game::isTheSnakeHead(int x, int y, const Player &player) const {
  return (x == player.current.x && y == player.current.y);
}

bool	Game::isOnBorder(int i, int j) const {
  return (i == 0 || j == 0 ||
	  i == _map.size.h - 1 ||
	  j == _map.size.w - 1);
}

bool	Game::isOnline() const {
  return (!!_sock);
}

Map		*Game::getMap() {
  return (&_map);
}

AGraphic	*Game::getGraph() const {
  return (_graph);
}

size_t	Game::getScoreForPlayer(Player *player) const {
  return (player->score);
}
