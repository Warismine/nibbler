#include <GL/glut.h>
#include <GL/freeglut_ext.h>
#include <sstream>
#include "LibOGL.hh"

#include <unistd.h>
#include <iostream>
#include <string>

extern "C"
{
  AGraphic	*getInstance(const Map &map, kevent current_event) {
    return (new LibOGL(map, current_event));
  }
}

void	display()
{
  glutSwapBuffers();
}

static kevent g_event = NONE;

void	arrowevent(int key, int, int)
{
  switch (key)
    {
    case GLUT_KEY_LEFT:
      g_event = LEFT;
      break;
    case GLUT_KEY_RIGHT:
      g_event = RIGHT;
      break;
    case GLUT_KEY_UP:
      g_event = UP;
      break;
    case GLUT_KEY_DOWN:
      g_event = DOWN;
      break;
    }
}

void	keyevent(unsigned char key, int, int)
{
  switch (key)
    {
    case 'q':
      g_event = QUIT;
      break;
    case 'Q':
      g_event = QUIT;
      break;
    case '1':
      g_event = SWITCH1;
      break;
    case '2':
      g_event = SWITCH2;
      break;
    case '3':
      g_event = SWITCH3;
      break;
    }
}

LibOGL::LibOGL(const Map &map, kevent event) :
  AGraphic(map, event)
{
  static int ac = 0;
  t_pos p = {0, 0};
  
  g_event = event;
  glutInit(&ac, NULL);
  glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
  glutInitWindowSize(1080, 1080);
  glutInitWindowPosition(500, 0);
  glutCreateWindow("OpenGL Nibbler");
  _ground = loadTexture(GL_IMG("disco1.png"));
  _nazi = loadTexture(GL_IMG("symbol.png"));
  _illuminati = loadTexture(GL_IMG("symbol3.png"));
  _aigle = loadTexture(GL_IMG("symbol2.png"));
  _croixDeFer = loadTexture(GL_IMG("symbol2.png"));
  _snake = loadTexture(GL_IMG("zebre.jpg"));
  glClearColor(0.3, 0.1, 0.1, 0.0);
  glutDisplayFunc(&display);
  glutIgnoreKeyRepeat(true);
  glutKeyboardFunc(&keyevent);
  glutSpecialFunc(&arrowevent);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluPerspective(90, 1, 1, 1000);
  glMatrixMode(GL_MODELVIEW);
  glEnable(GL_DEPTH_TEST);
  Move(p);
}

LibOGL::~LibOGL()
{}

void	LibOGL::affScore() const
{
  std::ostringstream	oss;

  oss << "Score : " << _score;
  glColor3f(1, 1, 1);
  glRasterPos3f(0, 1, -1);
  glutBitmapString(GLUT_BITMAP_9_BY_15,
  		   reinterpret_cast<const unsigned char*>(oss.str().c_str()));
}

void	LibOGL::printCube(int x, int y) const
{
  glPushMatrix();
  glTranslatef(x, 0.0f, y);
  glutSolidCube(1.0);
  glPopMatrix();
}

void	LibOGL::printSphere(int x, int y, float r) const
{
  glPushMatrix();
  glTranslatef(x, 0.0f, y);
  glutSolidSphere(r, 100.0, 100.0);
  glPopMatrix();
}

void	LibOGL::changeSnakeColor(content part, unsigned char num) const
{
  switch (num)
    {
    case 1:
      if (part == SNAKE_HEAD)
	glColor3f(0.0, 1.0, 0.0);
      else if (part == SNAKE_TAIL)
	glColor3f(0.0, 0.6, 0.0);
      else
	glColor3f(0.0, 0.8, 0.0);
      break ;
    case 2:
      if (part == SNAKE_HEAD)
	glColor3f(0.0, 0.0, 1.0);
      else if (part == SNAKE_TAIL)
	glColor3f(0.0, 0.0, 0.6);
      else
	glColor3f(0.0, 0.0, 0.8);
      break ;
    case 3:
      if (part == SNAKE_HEAD)
	glColor3f(1.0, 0.0, 0.0);
      else if (part == SNAKE_TAIL)
	glColor3f(0.6, 0.0, 0.0);
      else
	glColor3f(0.8, 0.0, 0.0);
      break ;
    case 4:
      if (part == SNAKE_HEAD)
	glColor3f(0.0, 1.0, 1.0);
      else if (part == SNAKE_TAIL)
	glColor3f(0.0, 0.6, 0.6);
      else
	glColor3f(0.0, 0.8, 0.8);
    }
}

void	LibOGL::TextureMode(bool flag) const
{
  if (flag)
    {
      glEnable(GL_TEXTURE_2D);
      glEnable(GL_TEXTURE_GEN_S);
      glEnable(GL_TEXTURE_GEN_T);
    }
  else
    {
      glDisable(GL_TEXTURE_GEN_S);
      glDisable(GL_TEXTURE_GEN_T);
      glDisable(GL_TEXTURE_2D);
    }
}

void	LibOGL::Move(const t_pos &)
{
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glLoadIdentity();
  gluLookAt(_map->size.w / 2 - 0.5, MAX(_map->size.w, _map->size.h) / 2 + 1,
  	    _map->size.h * 1.25,
  	    _map->size.w / 2 - 0.5, -1.0, _map->size.h * 0.75,
  	    0.0, 1.0, 0.0);
  glPushMatrix();
  glTranslatef(_map->size.w / 2 - 0.5, -1.0f, _map->size.h / 2- 0.5);
  glScalef(_map->size.w, 1.0, _map->size.h);
  TextureMode(true);
  glColor3f(1, 1, 1);
  glBindTexture(GL_TEXTURE_2D, _ground);
  glutSolidCube(1.0);
  TextureMode(false);
  glPopMatrix();
  for (int y = 0; y < _map->size.h; ++y)
    {
      for (int x = 0; x < _map->size.w; ++x)
  	{
  	  switch (_map->field[y][x].body.part)
  	    {
  	    case SNAKE_HEAD:
  	    case SNAKE_BODY:
  	    case SNAKE_TAIL:
	      TextureMode(true);
	      glBindTexture(GL_TEXTURE_2D, _snake);
	      changeSnakeColor(_map->field[y][x].body.part,
			       _map->field[y][x].body.player);
	      printCube(x, y);
	      TextureMode(false);
  	      break;
  	    case WALL:
	      TextureMode(true);
	      glBindTexture(GL_TEXTURE_2D, (x % 2 + y % 2 == 1 ? _illuminati : _nazi));
  	      glColor3f(1, 0, 0);
	      printCube(x, y);
	      TextureMode(false);
  	      break;
  	    case FOOD:
  	      glColor3f(1, 1, 0);
	      printSphere(x, y, .4);
  	      break;
  	    case XFOOD:
	      TextureMode(true);
	      glBindTexture(GL_TEXTURE_2D, _aigle);
  	      glColor3f(1, 0.65, 0);
	      printSphere(x, y, .6);
	      TextureMode(false);
  	      break;
  	    case KIHOHA:
	      TextureMode(true);
	      glBindTexture(GL_TEXTURE_2D, _croixDeFer);
  	      glColor3f(1.0, 0.0, 0);
	      printSphere(x, y, 1);
	      TextureMode(false);
  	      break;
	    case EMPTY:;
	    case STRAIGHT:;
  	    }
  	}
    }
  affScore();
  glutMainLoopEvent();
  glutSwapBuffers();
  glutPostRedisplay();
}

kevent	LibOGL::getKeyEvent()
{
  glutMainLoopEvent();
  if (g_event != NONE)
    _current_event = g_event;
  return _current_event;
}

void	LibOGL::quit()
{
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glLoadIdentity();
  gluLookAt(_map->size.w / 2 - 0.5, MAX(_map->size.w, _map->size.h) / 2 + 1,
  	    _map->size.h * 1.25,
  	    _map->size.w / 2 - 0.5, -1.0, _map->size.h * 0.75,
  	    0.0, 1.0, 0.0);
  affScore();
  glutSwapBuffers();
  sleep(2);
}
