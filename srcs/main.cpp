#include <sstream>
#include <X11/Xlib.h>
#include "Game.hh"
#include "Socket.hh"

void		error_msg()
{
  std::cerr << "Usage: ./nibbler [:x] [:y] libxxx.so" << std::endl;
  std::cerr << "./nibbler libxxx.so join [:port] [:ip]" << std::endl;
  std::cerr << "./nibbler [:x [:y] libxxx.so create [:port] [:player_nb]" << std::endl;
  std::cerr << "[:player_nb] must be between 2 and 4" << std::endl;
  std::cerr << "[:x] and [:y] must be between 10 and 100" << std::endl;
}

int	        between_map_value(t_size &size)
{
  return ((size.w >= 10 && size.w <= 60) &&
	  (size.h >= 10 && size.h <= 60));
}

int	        join_game(char **argv) {
  std::string	arg = argv[1];
  Socket	*sock;
  
  if (arg != "join")
    {
      error_msg();
      return (1);
    }
  try { sock = new Socket(argv[2], argv[3]); }
  catch (SocketException::GetprotobynameFailed &e) {
    std::cerr << e.what() << std::endl;
    return (1);
  }
  catch (SocketException::SocketFailed &e) {
    std::cerr << e.what() << std::endl;
    return (1);
  }
  try { new Game(argv[0], sock); }
  catch (SocketException::ConnectFailed &e) {
    std::cerr << e.what() << std::endl;
    return (1);
  }
  catch (SocketException::SocketClose &e) {
    std::cerr << e.what() << std::endl;
    return (-1);
  }
  catch (DLLoaderException::DLCloseFailed &e) {
    std::cerr << e.what() << std::endl;
    return (-1);
  }
  catch (DLLoaderException::DLSymFailed &e) {
    std::cerr << e.what() << std::endl;
    return (-1);
  }
  catch (DLLoaderException::DLOpenFailed &e) {
    std::cerr << e.what() << std::endl;
    return (-1);
  }
  return (0);
}

int	        create_game(t_size &size, char **argv) {
  int		num;
  std::istringstream is;
  std::string	arg = argv[1];
  Socket	*sock;

  is.str(argv[3]);
  is >> num;  
  if (arg != "create" || (num < 2 || num > 4))
    {
      error_msg();
      return (1);
    }
  try { sock = new Socket(argv[2]); }
  catch (SocketException::GetprotobynameFailed &e) {
    std::cerr << e.what() << std::endl;
    return (1);
  }
  catch (SocketException::SocketFailed &e) {
    std::cerr << e.what() << std::endl;
    return (1);
  }
  try { new Game(size, argv[0], sock, num); }
  catch (SocketException::BindFailed &e) {
    std::cerr << e.what() << std::endl;
    return (-1);
  }
  catch (SocketException::ListenFailed &e) {
    std::cerr << e.what() << std::endl;
    return (-1);
  }
  catch (SocketException::AcceptFailed &e) {
    std::cerr << e.what() << std::endl;
    return (-1);
  }
  catch (SocketException::SocketClose &e) {
    std::cerr << e.what() << std::endl;
    return (-1);
  }
  catch (DLLoaderException::DLCloseFailed &e) {
    std::cerr << e.what() << std::endl;
    return (-1);
  }
  catch (DLLoaderException::DLSymFailed &e) {
    std::cerr << e.what() << std::endl;
    return (-1);
  }
  catch (DLLoaderException::DLOpenFailed &e) {
    std::cerr << e.what() << std::endl;
    return (-1);
  }
  return (0);
}

int		main(int argc, char **argv) {
  t_size	size;
  std::istringstream av[2];

  XInitThreads();
  if (argc != 4 && argc != 7 && argc != 5)
    {
      error_msg();
      return (1);
    }
  if (argc == 5)
    return (join_game(&argv[1]));
  av[0].str(argv[1]);
  av[1].str(argv[2]);
  av[0] >> size.w;
  av[1] >> size.h;
  if (!between_map_value(size))
    {
      error_msg();
      return (1);
    }
  if (argc == 4)
    {
      new Game(size, argv[3]);
      return (0);
    }
  else
    return (create_game(size, &argv[3]));
  return (0);
}
