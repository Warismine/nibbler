#include <ncurses.h>
#include <sstream>
#include "Ncurses.hh"
#include "NcursesException.hpp"

Ncurses::Ncurses(const Map &map, kevent event) : AGraphic(map, event)
{
  if (initscr() == NULL)
    throw (NcursesException::InitScrFailed());
  if (keypad(stdscr, true) == ERR)
    throw (NcursesException::KeypadFailed());
  if (raw() == ERR)
    throw (NcursesException::RawFailed());
  if (start_color() == ERR)
    throw (NcursesException::StartColorFailed());
  setScore(0);
}

Ncurses::~Ncurses()
{
  if (endwin() == ERR)
    throw (NcursesException::EndWinFailed());
}

Ncurses		&Ncurses::operator=(const Ncurses &other)
{
  _get = other.getGet();
  _pos = other.getPos();
  return (*this);
}

void		Ncurses::Move(const t_pos &current)
{
  _pos = current;
  dispMap();
  dispScore();
}

void		Ncurses::dispMap() const
{
  clear();
  refresh();
  for (int x = 0; x < _map->size.h; ++x)
    {
      for (int y = 0; y < _map->size.w; ++y)
	{
	  if (_map->field[x][y].body.part != EMPTY)
	    dispCase(x, y, _map->field[x][y]);
	}
    }
  refresh();
}

void		Ncurses::dispCase(int x, int y, const Block &block) const
{
  int		player;

  player = block.body.player - 1;
  static const t_color	tab[4] = {
    {2, COLOR_GREEN},
    {4, COLOR_BLUE},
    {1, COLOR_RED},
    {3, COLOR_YELLOW},
  };
  switch (block.body.part)
    {
    case SNAKE_HEAD :
      init_pair(7, COLOR_BLACK, COLOR_WHITE);
      attron(COLOR_PAIR(7));
      mvprintw(x, y, " ");
      attroff(COLOR_PAIR(7));
      break ;
    case SNAKE_BODY :
      init_pair(tab[player].pair, COLOR_BLACK, tab[player].color);
      attron(COLOR_PAIR(tab[player].pair));
      mvprintw(x, y, " ");
      attroff(COLOR_PAIR(tab[player].pair));
      break ;
    case SNAKE_TAIL:
      init_pair(5, COLOR_BLACK, COLOR_MAGENTA);
      attron(COLOR_PAIR(5));
      mvprintw(x, y, " ");
      attroff(COLOR_PAIR(5));
      break ;
    case WALL :
      init_pair(1, COLOR_BLACK, COLOR_RED);
      attron(COLOR_PAIR(1));
      mvprintw(x, y, " ");
      attroff(COLOR_PAIR(1));
      break ;
    case FOOD :
      init_pair(6, COLOR_BLACK, COLOR_CYAN);
      attron(COLOR_PAIR(6));
      mvprintw(x, y, " ");
      attroff(COLOR_PAIR(6));
      break ;
    case XFOOD :
      init_pair(3, COLOR_BLACK, COLOR_YELLOW);
      attron(COLOR_PAIR(3));
      mvprintw(x, y, " ");
      attroff(COLOR_PAIR(3));
      break ;
    case KIHOHA :
      init_pair(6, COLOR_BLACK, COLOR_CYAN);
      attron(COLOR_PAIR(6));
      mvprintw(x, y, " ");
      attroff(COLOR_PAIR(6));
      break ;
    default :
      break ;
    }
}

void			Ncurses::dispScore() const
{
  std::stringstream	ss;

  ss << _score;
  mvprintw(0, 0, ss.str().c_str());
  refresh();
}

void			Ncurses::quit()
{
  int			x;
  int			y;
  std::stringstream	ss;

  ss << _score;
  getmaxyx(stdscr, x, y);
  clear();
  refresh();
  mvprintw(x/3 , y/3, "You are Dead. Press 'q' to leave the game.\n");
  mvprintw(x/3 + 2, y/3, "Score : ");
  mvprintw(x/3 + 2, y/3 + 8, ss.str().c_str());
  while (getch() != 113)
    ;
}

/* GETTERS */

extern "C"
{
  AGraphic	*getInstance(const Map &map, kevent current_event) {
    return (new Ncurses(map, current_event));
  }
}

const t_pos	&Ncurses::getPos() const
{
  return (_pos);
}

int		Ncurses::getGet() const
{
  return (_get);
}

bool		Ncurses::checkEvent(const kevent &e1, const kevent &e2) const
{
  if (e1 == e2)
    return (true);
  return (false);
}

kevent			Ncurses::getKeyEvent()
{
  kevent		tmp;

  timeout(1);
  _get = getch();
  switch(_get)
    {
    case 49 :
      tmp = SWITCH1;
      break ;
    case 50 :
      tmp = SWITCH2;
      break ;
    case 51 :
      tmp = SWITCH3;
      break ;
    case 113 :
      tmp = QUIT;
      break ;
    case KEY_LEFT :
      tmp = LEFT;
      break ;
    case 27 :
      tmp = QUIT;
      break ;
    case KEY_RIGHT :
      tmp = RIGHT;
      break ;
    case KEY_UP :
      tmp = UP;
      break ;
    case KEY_DOWN :
      tmp = DOWN;
      break;
    default:
      tmp = NONE;
      break ;
    }
  if (tmp != NONE)
    _current_event = tmp;
  return (_current_event);
}
