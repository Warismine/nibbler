#include <cstdlib>
#include "Game.hh"

bool	Game::getPart(t_pos &pos, int n, const Player *player) {
  int	x;
  int	y = pos.y - 1;

  while (y <= pos.y + 1)
    {
      x = pos.x - 1;
      while (x <= pos.x + 1)
	{
	  if (_map.field[y][x].body.player == player->player_nb &&
	      _map.field[y][x].body.num - 1 == n &&
	      ((pos.y != y) || (pos.x != x)))
	    {
	      pos.y = y;
	      pos.x = x;
	      return (true);
	    }
	  ++x;
	}
      ++y;
    }
  return (false);
}

void	*Game::putFoodOnMap(void *arg) {
  int	x;
  int	y;
  bool	set = false;
  Map	*map;

  map = static_cast<Map *>(arg);
  while (!set) {
    y = (rand() * time(0)) % map->size.h;
    x = (rand() * time(0)) % map->size.w;
    if (map->field[y][x].body.part == EMPTY)
      {
	map->field[y][x].body.part = FOOD;
	set = true;
      }
  }
  return (0);
}

void	*Game::putXtraFoodOnMap(void *arg) {
  int	x;
  int	y;
  bool	set = false;
  Map	*map;

  map = static_cast<Map *>(arg);
  if (((rand() * time(0)) % 15))
    return (0);
  while (!set) {
    y = (rand() * time(0)) % map->size.h;
    x = (rand() * time(0)) % map->size.w;
    if (map->field[y][x].body.part == EMPTY)
      {
	map->field[y][x].body.part = XFOOD;
	set = true;
      }
  }
  return (0);
}


void	Game::setScoreWithFood(content k, Player *player) {
  if (k == FOOD)
    player->score += 10;
  else if (k == XFOOD)
    player->score += 30;
}

bool	Game::launchBallToStorm() {
  int   p1 = (rand() * time(0)) % 4;
  int	i = 0;
  int	x;
  int	y;

  while (_players[p1].data.is_dead && i < 20)
    {
      p1 = (rand() * time(0)) % 4;
      ++i;
    }
  if (i == 20)
    return (false);
  for (int n = 0; n < 20; ++n) {
    y = (rand() * time(0)) % _map.size.h;
    x = (rand() * time(0)) % _map.size.w;
    if (_map.field[y][x].body.part == EMPTY)
      {
	kevent event = _players[p1].event;
	_map.field[_players[p1].current.y][_players[p1].current.x].direction = NONE;
	_map.field[_players[p1].current.y][_players[p1].current.x].body.part = EMPTY;
	_map.field[_players[p1].current.y][_players[p1].current.x].body.num = 0;
	_map.field[_players[p1].current.y][_players[p1].current.x].body.player = 0;

	_map.field[y][x].direction = event;
	_map.field[y][x].body.part = SNAKE_HEAD;
	_map.field[y][x].body.num = 1;
	_map.field[y][x].body.player = p1 + 1;
	_players[p1].current.x = x;
	_players[p1].current.y = y;
	return (true);
      }
  }
  return (false);
}

void	*Game::launchBall(void *arg) {
  Player *player = static_cast<Player *>(arg);
  Game	*game = player->snake_player->game;
  Map	*map = game->getMap();
  kevent event = player->event;
  int	random = (rand() * time(0)) % 10;

  if (!random && game->launchBallToStorm())
    return (0);
  switch (event) {
  case LEFT:
    Game::horizontalLaunch(map, event, 1, player->current);
    break ;
  case RIGHT:
    Game::horizontalLaunch(map, event, -1, player->current);
    break ;
  case UP:
    Game::verticalLaunch(map, event, 1, player->current);
    break ;
  case DOWN:
    Game::verticalLaunch(map, event, -1, player->current);
    break ;
  default:
    return (0);
  }
  return (0);
}

void	Game::horizontalLaunch(Map *map, kevent event, int dir, const t_pos &pos) {
  int		i = 1;
  int		x = pos.x;
  int		y = pos.y;
  pthread_t	food_thread = 0;

  while (map->field[y][x - dir * i].body.part != WALL)
    {
      map->field[y][x - dir * i].direction = event;
      if (i - 1 != 0)
	{
	  map->field[y][x - dir * (i - 1)].body.part = EMPTY;
	  map->field[y][x - dir * (i - 1)].body.num = 0;
	  map->field[y][x - dir * (i - 1)].body.player = 0;
	}
      if (map->field[y][x - dir * i].body.part == FOOD)
	if (pthread_create(&food_thread, NULL, &Game::putFoodOnMap, map) != 0)
	  {
	    std::cerr << "Couldn't be able to create thread" << std::endl;
	    return ;
	  }
      map->field[y][x - dir * i].body.part = KIHOHA;
      ++i;
      usleep(35000);
    }
  map->field[y][x - dir * (i - 1)].body.part = EMPTY;
  if (food_thread != 0)
    pthread_join(food_thread, NULL);
}

void	Game::verticalLaunch(Map *map, kevent event, int dir, const t_pos &pos) {
  int		i = 1;
  int		x = pos.x;
  int		y = pos.y;
  pthread_t	food_thread = 0;

  while (map->field[y - dir * i][x].body.part != WALL)
    {
      map->field[y - dir * i][x].direction = event;
      if (i - 1 != 0)
	{
	  map->field[y - dir * (i - 1)][x].body.part = EMPTY;
	  map->field[y - dir * (i - 1)][x].body.num = 0;
	  map->field[y - dir * (i - 1)][x].body.player = 0;
	}
      if (map->field[y - dir * i][x].body.part == FOOD)
	if (pthread_create(&food_thread, NULL, &Game::putFoodOnMap, map) != 0)
	  {
	    std::cerr << "Couldn't be able to create thread" << std::endl;
	    return ;
	  }
      map->field[y - dir * i][x].body.part = KIHOHA;
      ++i;
      usleep(35000);
    }
  map->field[y - dir * (i - 1)][x].body.part = EMPTY;
  if (food_thread != 0)
    pthread_join(food_thread, NULL);
}

void		Game::increaseSnakeSize(kevent direction, Player *player,
					content food, t_pos &pos) {
  int		x = player->current.x;
  int		y = player->current.y;
  pthread_t	food_thread;
  pthread_t	xfood_thread;
  pthread_t	kihoha;

  setScoreWithFood(food, player);
  if (food == FOOD)
    {
      if (pthread_create(&food_thread, NULL, &Game::putFoodOnMap, &_map) != 0)
	{
	  std::cerr << "Couldn't be able to create thread" << std::endl;
	  player->data.is_dead = true;
	  pthread_exit(NULL) ;
	}
    }
  else if (food == XFOOD && _sock)
    if (pthread_create(&kihoha, NULL, &Game::launchBall, player) != 0)
      {
	player->data.is_dead = true;
	std::cerr << "Couldn't be able to create thread" << std::endl;
        pthread_exit(NULL);
      }
  if (pthread_create(&xfood_thread, NULL, &Game::putXtraFoodOnMap, &_map) != 0)
    {
      player->data.is_dead = true;
      std::cerr << "Couldn't be able to create thread" << std::endl;
      pthread_exit(NULL);
    }
  if (player->player_nb == 1)
    _graph->setScore(getScoreForPlayer(player));
  _map.field[pos.y][pos.x].body.part = SNAKE_BODY;
  _map.field[y][x].body.player = _map.field[pos.y][pos.x].body.player;
  ++_map.field[pos.y][pos.x].body.num;
  _map.field[pos.y][pos.x].direction = direction;
  while (getPart(pos, _map.field[pos.y][pos.x].body.num - 1, player))
    ++_map.field[pos.y][pos.x].body.num;
  return ;
}

void		Game::updatePosition(bool find_food, kevent direction, Player *player) {
  t_pos		pos;
  int		x = player->current.x;
  int		y = player->current.y;
  content	food;

  food = _map.field[y][x].body.part;
  _map.field[y][x].direction = direction;
  _map.field[y][x].body.part = SNAKE_HEAD;
  _map.field[y][x].body.num = 1;
  _map.field[y][x].body.player = player->player_nb;
  pos.x = x;
  pos.y = y;
  getPart(pos, 0, player);
  if (find_food)
    return (increaseSnakeSize(direction, player, food, pos));
  x = pos.x;
  y = pos.y;
  _map.field[y][x].direction = direction;
  while (getPart(pos, _map.field[y][x].body.num, player))
    {
      _map.field[y][x].body.part = _map.field[pos.y][pos.x].body.part;
      _map.field[y][x].body.num = _map.field[pos.y][pos.x].body.num;
      x = pos.x;
      y = pos.y;
    }
  _map.field[y][x].body.part = EMPTY;
  _map.field[y][x].direction = NONE;
  _map.field[y][x].body.num = 0;
}

void	Game::changeSnakeSpeed(Player *player) const {
  if (player->player_nb == 1) {
    if (player->speed >= 200)
      player->speed -= 10; }
  else
    if (player->speed >= 20)
      player->speed -= 10;
}
