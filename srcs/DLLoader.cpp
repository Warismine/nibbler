#include "DLLoader.hh"

template <typename X>
DLLoader<X> &DLLoader<X>::Instance() {
  static DLLoader<X> d;
  return (d);
}

template<typename X>
void	DLLoader<X>::closeLib()
{
  if (!handler.empty())
    for (std::map<std::string, void *>::const_iterator it =
	   handler.begin(); it != handler.end(); ++it)
      {
	if (_currentHandler == it->second)
	  {
	    if (dlclose(_currentHandler))
	      throw DLLoaderException::DLCloseFailed();
	    handler.erase(it->first);
	    return ;
	  }
      }
}

template<typename X>
void		DLLoader<X>::initWithLib(const std::string &lib)
{
  if (!handler.empty())
    for (std::map<std::string, void *>::const_iterator it =
	   handler.begin(); it != handler.end(); ++it)
      {
	if (lib == it->first)
	  {
	    _currentHandler = it->second;
	    return ;
	  }
      }
  if ((_currentHandler = dlopen(lib.c_str(), RTLD_NOW)) == NULL)
    {
      std::cerr << dlerror() << std::endl;
      throw DLLoaderException::DLOpenFailed();
    }
  handler[lib] = _currentHandler;
}

template<typename X>
void			DLLoader<X>::changeLib(const std::string &lib)
{
  if (!handler.empty())
    for (std::map<std::string, void *>::const_iterator it =
	   handler.begin(); it != handler.end();++it)
      {
	if (it->first == lib)
	  {
	    _currentHandler = it->second;
	    return ;
	  }
      }
  return (initWithLib(lib));
}

template<typename X>
void		*DLLoader<X>::getFunctionFromLib(const std::string &sym) const {
  void		*func;

  func = dlsym(_currentHandler, sym.c_str());
  if (func == NULL)
    {
      std::cerr << dlerror() << std::endl;
      throw DLLoaderException::DLSymFailed();
    }
  return (func);
}

template class DLLoader<AGraphic *>;
