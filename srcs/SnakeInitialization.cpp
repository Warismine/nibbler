#include "Game.hh"

void	Game::initMap() {
  int	i;
  int	j;

  for (i = 0; i < _map.size.h; ++i)
    {
      for (j = 0; j < _map.size.w; ++j)
	{
	  _map.field[i][j].direction = NONE;
	  if (isOnBorder(i, j))
	    {
	      _map.field[i][j].body.part = WALL;
	      _map.field[i][j].body.num = 0;
	      _map.field[i][j].body.player = 0;
	    }
	  else
	    {
	      _map.field[i][j].body.part = EMPTY;
	      _map.field[i][j].body.num = 0;
	      _map.field[i][j].body.player = 0;
	    }
	}
    }
}

void	Game::initSnake(Player &player, int num) {
  switch (num) {
  case 0:
    return (snakeOne(player));
  case 1:
    return (snakeTwo(player));
  case 2:
    return (snakeThree(player));
  case 3:
    return (snakeFour(player));
  default:
    return ;
  }
  return ;
}

void	Game::initPlayer(int num) {
  _players[num].player_nb = num + 1;
  _players[num].speed = 100000;
  _players[num].score = 0;
  _players[num].data.end_of_game = false;
  _players[num].snake_player = new SnakePlayer;
  _players[num].snake_player->game = this;
  _players[num].snake_player->player = &(_players[num]);
}

void	        Game::snakeOne(Player &player) {
  player.current.x = _map.size.w / 2 + 2;
  player.current.y = _map.size.h / 2 + 2;
  player.event = DOWN;

  _map.field[_map.size.h / 2 + 2][_map.size.w / 2 + 2].body.part = SNAKE_HEAD;
  _map.field[_map.size.h / 2 + 2][_map.size.w / 2 + 2].body.num = 1;
  _map.field[_map.size.h / 2 + 2][_map.size.w / 2 + 2].direction = DOWN;
  _map.field[_map.size.h / 2 + 2][_map.size.w / 2 + 2].body.player = player.player_nb;

  _map.field[_map.size.h / 2 + 1][_map.size.w / 2 + 2].body.part = SNAKE_BODY;
  _map.field[_map.size.h / 2 + 1][_map.size.w / 2 + 2].body.num = 2;
  _map.field[_map.size.h / 2 + 1][_map.size.w / 2 + 2].direction = DOWN;
  _map.field[_map.size.h / 2 + 1][_map.size.w / 2 + 2].body.player = player.player_nb;

  _map.field[_map.size.h / 2][_map.size.w / 2 + 2].body.part = SNAKE_BODY;
  _map.field[_map.size.h / 2][_map.size.w / 2 + 2].body.num = 3;
  _map.field[_map.size.h / 2][_map.size.w / 2 + 2].direction = DOWN;
  _map.field[_map.size.h / 2][_map.size.w / 2 + 2].body.player = player.player_nb;

  _map.field[_map.size.h / 2][_map.size.w / 2 + 1].body.part = SNAKE_TAIL;
  _map.field[_map.size.h / 2][_map.size.w / 2 + 1].body.num = 4;
  _map.field[_map.size.h / 2][_map.size.w / 2 + 1].direction = RIGHT;
  _map.field[_map.size.h / 2][_map.size.w / 2 + 1].body.player = player.player_nb;
}

void		Game::snakeTwo(Player &player) {
  player.current.x = _map.size.w / 2 - 2;
  player.current.y = _map.size.h / 2 - 2;
  player.event = UP;

  _map.field[_map.size.h / 2 - 2][_map.size.w / 2 - 2].body.part = SNAKE_HEAD;
  _map.field[_map.size.h / 2 - 2][_map.size.w / 2 - 2].body.num = 1;
  _map.field[_map.size.h / 2 - 2][_map.size.w / 2 - 2].direction = UP;
  _map.field[_map.size.h / 2 - 2][_map.size.w / 2 - 2].body.player = player.player_nb;

  _map.field[_map.size.h / 2 - 1][_map.size.w / 2 - 2].body.part = SNAKE_BODY;
  _map.field[_map.size.h / 2 - 1][_map.size.w / 2 - 2].body.num = 2;
  _map.field[_map.size.h / 2 - 1][_map.size.w / 2 - 2].direction = UP;
  _map.field[_map.size.h / 2 - 1][_map.size.w / 2 - 2].body.player = player.player_nb;

  _map.field[_map.size.h / 2][_map.size.w / 2 - 2].body.part = SNAKE_BODY;
  _map.field[_map.size.h / 2][_map.size.w / 2 - 2].body.num = 3;
  _map.field[_map.size.h / 2][_map.size.w / 2 - 2].direction = UP;
  _map.field[_map.size.h / 2][_map.size.w / 2 - 2].body.player = player.player_nb;

  _map.field[_map.size.h / 2][_map.size.w / 2 - 1].body.part = SNAKE_TAIL;
  _map.field[_map.size.h / 2][_map.size.w / 2 - 1].body.num = 4;
  _map.field[_map.size.h / 2][_map.size.w / 2 - 1].direction = LEFT;
  _map.field[_map.size.h / 2][_map.size.w / 2 - 1].body.player = player.player_nb;
}

void	        Game::snakeThree(Player &player) {
  player.current.x = _map.size.w / 2 - 1;
  player.current.y = _map.size.h / 2 + 3;
  player.event = LEFT;

  _map.field[_map.size.h / 2 + 3][_map.size.w / 2 - 1].body.part = SNAKE_HEAD;
  _map.field[_map.size.h / 2 + 3][_map.size.w / 2 - 1].body.num = 1;
  _map.field[_map.size.h / 2 + 3][_map.size.w / 2 - 1].direction = LEFT;
  _map.field[_map.size.h / 2 + 3][_map.size.w / 2 - 1].body.player = player.player_nb;

  _map.field[_map.size.h / 2 + 3][_map.size.w / 2].body.part = SNAKE_BODY;
  _map.field[_map.size.h / 2 + 3][_map.size.w / 2].body.num = 2;
  _map.field[_map.size.h / 2 + 3][_map.size.w / 2].direction = LEFT;
  _map.field[_map.size.h / 2 + 3][_map.size.w / 2].body.player = player.player_nb;

  _map.field[_map.size.h / 2 + 2][_map.size.w / 2].body.part = SNAKE_BODY;
  _map.field[_map.size.h / 2 + 2][_map.size.w / 2].body.num = 3;
  _map.field[_map.size.h / 2 + 2][_map.size.w / 2].direction = DOWN;
  _map.field[_map.size.h / 2 + 2][_map.size.w / 2].body.player = player.player_nb;

  _map.field[_map.size.h / 2 + 1][_map.size.w / 2].body.part = SNAKE_TAIL;
  _map.field[_map.size.h / 2 + 1][_map.size.w / 2].body.num = 4;
  _map.field[_map.size.h / 2 + 1][_map.size.w / 2].direction = DOWN;
  _map.field[_map.size.h / 2 + 1][_map.size.w / 2].body.player = player.player_nb;
}

void	        Game::snakeFour(Player &player) {
  player.current.x = _map.size.w / 2 + 1;
  player.current.y = _map.size.h / 2 - 3;
  player.event = RIGHT;

  _map.field[_map.size.h / 2 - 3][_map.size.w / 2 + 1].body.part = SNAKE_HEAD;
  _map.field[_map.size.h / 2 - 3][_map.size.w / 2 + 1].body.num = 1;
  _map.field[_map.size.h / 2 - 3][_map.size.w / 2 + 1].direction = RIGHT;
  _map.field[_map.size.h / 2 - 3][_map.size.w / 2 + 1].body.player = player.player_nb;

  _map.field[_map.size.h / 2 - 3][_map.size.w / 2].body.part = SNAKE_BODY;
  _map.field[_map.size.h / 2 - 3][_map.size.w / 2].body.num = 2;
  _map.field[_map.size.h / 2 - 3][_map.size.w / 2].direction = RIGHT;
  _map.field[_map.size.h / 2 - 3][_map.size.w / 2].body.player = player.player_nb;

  _map.field[_map.size.h / 2 - 2][_map.size.w / 2].body.part = SNAKE_BODY;
  _map.field[_map.size.h / 2 - 2][_map.size.w / 2].body.num = 3;
  _map.field[_map.size.h / 2 - 2][_map.size.w / 2].direction = UP;
  _map.field[_map.size.h / 2 - 2][_map.size.w / 2].body.player = player.player_nb;

  _map.field[_map.size.h / 2 - 1][_map.size.w / 2].body.part = SNAKE_TAIL;
  _map.field[_map.size.h / 2 - 1][_map.size.w / 2].body.num = 4;
  _map.field[_map.size.h / 2 - 1][_map.size.w / 2].direction = UP;
  _map.field[_map.size.h / 2 - 1][_map.size.w / 2].body.player = player.player_nb;
}
