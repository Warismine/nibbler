#include <sstream>
#include <unistd.h>
#include "SFMLGraphic.hh"

extern "C"
{
  AGraphic	*getInstance(const Map &map, kevent current_event) {
    return (new SFMLGraphic(map, current_event));
  }
}

SFMLGraphic::SFMLGraphic(const Map &map, kevent event) :
  AGraphic(map, event),
  _window(sf::VideoMode::getDesktopMode(), "Nibbler", sf::Style::Fullscreen),
  _bg(SFML_IMG("background.jpg")), _scale(1), _stop(false)
{
  sf::VideoMode tmp = sf::VideoMode::getDesktopMode();
  std::map<content, CenterSprite*>::iterator it;
  t_pos		pos = {0, 0};

  _font.loadFromFile(SFML_FONT("ka1.ttf"));
  _window.setKeyRepeatEnabled(false);
  _window.setVerticalSyncEnabled(true);
  _window.setMouseCursorVisible(false);
  _window.clear();
  _color.push_back(sf::Color(255, 255, 255));
  _color.push_back(sf::Color(0, 0, 255));
  _color.push_back(sf::Color(255, 0, 0));
  _color.push_back(sf::Color(0, 0, 0));
  _scale = static_cast<float>(tmp.width) / CASEW(map.size.w, 1);
  _scale = MIN(_scale, (static_cast<float>(tmp.height) - TOPBAR)
	       / CASEW(map.size.h, 1));
  _sprites[SNAKE_HEAD] = new CenterSprite(SFML_IMG("head.png"));
  _sprites[SNAKE_BODY] = new CenterSprite(SFML_IMG("body.png"));
  _sprites[SNAKE_TAIL] = new CenterSprite(SFML_IMG("tail.png"));
  _sprites[WALL] = new CenterSprite(SFML_IMG("wall.png"));
  _sprites[FOOD] = new CenterSprite(SFML_IMG("food.png"));
  _sprites[XFOOD] = new CenterSprite(SFML_IMG("db1.png"));
  _sprites[STRAIGHT] = new CenterSprite(SFML_IMG("bodyc.png"));
  _sprites[KIHOHA] = new CenterSprite(SFML_IMG("kihoha.png"));
  for (it = _sprites.begin(); it != _sprites.end(); ++it)
    it->second->setScale(_scale, _scale);
  _angle[LEFT] = 0;
  _angle[RIGHT] = 180;
  _angle[UP] = 90;
  _angle[DOWN] = 270;
  this->Move(pos);
}

void	SFMLGraphic::readFrames()
{
  Frame			*tmp;

  if (_buffer.loadFromFile(SFML_MOV("movie.wav")))
    {
      _music.setBuffer(_buffer);
      _music.play();
    }
  while (_frames.size() != 0)
    {
      tmp = _frames.front();
      if (_stop == false)
	{
	  _window.draw(tmp->getSprite());
	  _window.display();
	  usleep(45000);
	}
      _frames.pop_front();
      delete tmp;
    }
  _stop = true;
}

void	SFMLGraphic::printLoading()
{
  sf::Text		txt;

  _window.clear();
  txt.setPosition(0, 0);
  txt.setFont(_font);
  txt.setString("Loading...");
  txt.setCharacterSize(100);
  txt.setColor(sf::Color::White);
  _window.draw(txt);
  _window.display();
}

void	SFMLGraphic::playOpening()
{
  sf::Event		e;
  std::ostringstream	oss;
  Frame			*frame;
  sf::Thread		th(&SFMLGraphic::readFrames, this);
  Frame			start(SFML_IMG("start.jpg"));

  printLoading();
  for (int i = 1; i <= 3216; i += 2)
    {
      if (_window.pollEvent(e) && (PASS(e) || QUIT(e)))
	{
	  _stop = true;
	  break;
	}
      oss << SFML_MOV("img") << i << ".jpg";
      if (i == 351)
	th.launch();
      frame = new Frame(oss.str());
      _frames.push_back(frame);
      oss.str("");
      usleep(6500);
    }
  while (_stop == false)
    {
      _window.pollEvent(e);
      if (PASS(e) || QUIT(e))
	{
	  _stop = true;
	  break;
	}
    }
  th.wait();
  if (_buffer.loadFromFile(SFML_SND("load.wav")))
    {
      _music.setBuffer(_buffer);
      _music.play();
    }
  _window.draw(start.getSprite());
  _window.display();
  while (_window.waitEvent(e))
    {
      if (PASS(e) || QUIT(e) || e.joystickButton.button == 9)
	{
	  _stop = true;
	  break ;
	}
    }
}

SFMLGraphic::~SFMLGraphic()
{
  std::map<content, CenterSprite*>::iterator	it;

  for (it = _sprites.begin(); it != _sprites.end(); ++it)
    {
      delete it->second;
    }
  _window.close();
}

void		SFMLGraphic::affScore()
{
  std::ostringstream	oss;
  sf::RectangleShape	rect;
  sf::Text		txt;

  rect.setSize(sf::Vector2f(sf::VideoMode::getDesktopMode().width, TOPBAR));
  rect.setFillColor(sf::Color::Black);
  rect.setPosition(0, 0);
  _window.draw(rect);
  oss << "Score : " << _score;
  txt.setPosition(0, 0);
  txt.setFont(_font);
  txt.setString(oss.str());
  txt.setCharacterSize(23);
  txt.setColor(sf::Color::White);
  _window.draw(txt);
}


void		SFMLGraphic::Move(const t_pos &)
{
  print_bg();
  for (int y = 0; y < _map->size.h; ++y)
    {
      for (int x = 0; x < _map->size.w; ++x)
	{
	  if (_map->field[y][x].body.part != EMPTY)
	    drawAtCase(x, y, _map->field[y][x]);
	}
    }
  affScore();
  _window.display();
}

float		SFMLGraphic::getPrevDirection(int x, int y, kevent dir)
{
  switch(dir)
    {
    case LEFT:
      if (_map->field[y + 1][x].direction == UP)
	return (270);
      return (0);
    case RIGHT:
      if (_map->field[y + 1][x].direction == UP)
	return (180);
      return (90);
    case UP:
      if (_map->field[y][x + 1].direction == LEFT)
	return (90);
      return (0);
    case DOWN:
      if (_map->field[y][x + 1].direction == LEFT)
	return (180);
      return (270);
    default:
      return (45);
    }
}

void		SFMLGraphic::drawAtCase(int x, int y, const Block &block)
{
  int		straight = 1;
  float		dirX = _angle[block.direction];
  float		rot = 45;

  if (block.body.part == SNAKE_BODY)
    {
      switch (block.direction)
	{
	case LEFT:
	  straight = _map->field[y][x + 1].direction == LEFT;
	  break ;
	case RIGHT:
	  straight = _map->field[y][x - 1].direction == RIGHT;
	  break ;
	case UP:
	  straight = _map->field[y + 1][x].direction == UP;
	  break ;
	case DOWN:
	  straight = _map->field[y - 1][x].direction == DOWN;
	  break ;
	default:
	  straight = 0;
	}
    }
  if (straight && _sprites[block.body.part])
    {
      _sprites[block.body.part]->setPosition(CASEW(x, _scale),
					     CASEH(y, _scale));
      _sprites[block.body.part]->setRotation(dirX);
      if (block.body.part == SNAKE_TAIL || block.body.part == SNAKE_BODY ||
	  block.body.part == SNAKE_HEAD)
	_sprites[block.body.part]->setColor(_color[block.body.player - 1]);
      _window.draw(*(_sprites[block.body.part]));
    }
  if (!straight)
    {
      _sprites[STRAIGHT]->setPosition(CASEW(x, _scale),
				      CASEH(y, _scale));
      rot = getPrevDirection(x, y, block.direction);
      _sprites[STRAIGHT]->setRotation(rot);
	_sprites[STRAIGHT]->setColor(_color[block.body.player - 1]);
      _window.draw(*(_sprites[STRAIGHT]));
    }
}

void		SFMLGraphic::quit()
{
  sf::Event		e;
  sf::Text		txt;
  std::ostringstream	oss;

  _window.clear();
  oss << "Final Score : " << _score;
  txt.setPosition(0, 0);
  txt.setFont(_font);
  txt.setString(oss.str());
  txt.setCharacterSize(100);
  txt.setColor(sf::Color::White);
  _window.draw(txt);
  _window.display();
  do
    {
      _window.waitEvent(e);
    } while (!QUIT(e));
}

kevent		SFMLGraphic::getKeyEvent()
{
  sf::Event	e;
  kevent	tmp;

   _window.pollEvent(e);
  switch (e.type)
    {
    case sf::Event::Closed:
      tmp = QUIT;
      break ;
    case sf::Event::JoystickMoved:
      tmp = NONE;
      if (e.joystickMove.axis == sf::Joystick::X)
	tmp = (e.joystickMove.position == -100 ? LEFT : RIGHT);
      else if (e.joystickMove.axis == sf::Joystick::Y)
	tmp = (e.joystickMove.position == -100 ? UP : DOWN);
      break ;
    case sf::Event::JoystickButtonPressed:
      tmp = NONE;
      if (QUIT(e))
	tmp = QUIT;
      break;
    case sf::Event::KeyPressed:
      switch (e.key.code)
	{
	case sf::Keyboard::Q:
	  tmp = QUIT;
	  break ;
	case sf::Keyboard::Left:
	  tmp = LEFT;
	  break ;
	case sf::Keyboard::Right:
	  tmp = RIGHT;
	  break ;
	case sf::Keyboard::Up:
	  tmp = UP;
	  break ;
	case sf::Keyboard::Down:
	  tmp = DOWN;
	  break ;
	case sf::Keyboard::Num1:
	  tmp = SWITCH1;
	  break ;
	case sf::Keyboard::Num2:
	  tmp = SWITCH2;
	  break ;
	case sf::Keyboard::Num3:
	  tmp = SWITCH3;
	  break ;
	default:
	  tmp = NONE;
	  break ;
	}
      break ;
    default:
      tmp = NONE;
      break ;
    }
  if (tmp != NONE)
    _current_event = tmp;
  return (_current_event);
}

void		SFMLGraphic::print_bg()
{
  sf::VideoMode tmp = sf::VideoMode::getDesktopMode();
  int		bgW = _bg.getW();
  int		bgH = _bg.getH();
  unsigned int	curX = 0;
  unsigned int	curY = TOPBAR;

  for (_bg.setPosition(0, TOPBAR); curX < tmp.width; curX += bgW)
    {
      curY = TOPBAR;
      for (_bg.setPosition(curX, TOPBAR); curY < tmp.height; curY += bgH)
	{
	  _bg.setPosition(curX, curY);
	  _window.draw(_bg);
	}
    }
}
