##
## MakeFile for makefile in /home/abollo_h/rendu/cpp_nibbler
##
## Made by heru abollo
## Login   <abollo_h@epitech.net>
##
## Started on  Wed  18 12:33:14 2015 heru abollo
## Last update Thu Apr  2 15:54:22 2015 Emile TAWFIK
##


SRCS    =	./srcs/Game.cpp \
		./srcs/main.cpp \
	        ./srcs/DLLoader.cpp \
		./srcs/Socket.cpp \
		./srcs/SnakeInitialization.cpp \
		./srcs/UpdateMove.cpp \

NAME	= nibbler

LIB1	= ./lib_nibbler_sfml.so
LIB2	= ./lib_nibbler_ncurses.so
LIB3	= ./lib_nibbler_opengl.so

OBJ	= $(SRCS:.cpp=.o)

CC	= g++

CPPFLAGS     += -Wall -Wextra -Werror

CPPFLAGS     += -I./includes/ -ldl -lX11 -lpthread

CP	= cp

RM	= rm -f

all: 		LIB1 LIB2 LIB3 $(NAME)

$(NAME): 	 $(OBJ)
	@$(CC) -o $(NAME) $(OBJ) $(CPPFLAGS)

LIB1:
	@cd srcs/SFML && make

LIB2:
	@cd srcs/Ncurses && make

LIB3:
	@cd srcs/OpenGL && make

clean:
	@$(RM) $(OBJ)
	@cd srcs/SFML && make clean
	@cd srcs/Ncurses && make clean
	@cd srcs/OpenGL && make clean

fclean:	clean
	@$(RM) $(NAME)
	@cd srcs/SFML && make fclean
	@cd srcs/Ncurses && make fclean
	@cd srcs/OpenGL && make fclean

re: fclean all

.PHONY: all clean fclean re
